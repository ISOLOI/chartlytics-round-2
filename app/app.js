"use strict";
import React from 'react';
import { render } from 'react-dom';
import resources from './data.js';
import { Table } from 'react-bootstrap';

export default class App extends React.Component {

    transformdata(resources) {
        resources.sort(function (a, b) {
            return a.order - b.order;
        });
        return resources.map(this.processData, { previousType: null }).filter(this.filterData);
    }

    // Look at each object in resources and do something for each. 
    processData(resource, i, resources) {


        if (resource.type !== 'Person' || this.previousType == 'Person') {
            this.previousType = resource.type;
            return resource;
        }



        let order = 0;
        let people = [];

        // Look at the next objects and aggregate order and people, for all of those objects until we find an object that is NOT of type Person
        for (i; i < resources.length; i++) {
            if (resources[i].type !== 'Person')
                break;
            order = Math.max(resources[i].order, resource.order);
            people.push(resources[i].name);
        }

        this.previousType = resource.type;

        return {
            type: "Person",
            order: order,
            people: people
        };

    }


    filterData(resource, i, resources) {
        // Automatically return true for any object that is not a person (should be included in final output array)
        if (resource.type !== 'Person')
            return true;

        // Look at each object and return false for all duplicate "names" (already been aggregated)
        if (i != 0 && (resources[i - 1].type === 'Person' || resources[i - 1].hasOwnProperty('people')))
            return false;

        return true;

    }


    render() {
        console.log(resources);
        let mydata = this.transformdata(resources);

        return (
            <div>
                <Table responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            mydata.map((obj, index) => {
                                return <tr>
                                    <td>{(index + 1)}</td>
                                    {
                                        Object.keys(obj).map((key, index) => {
                                            if (Array.isArray(obj[key]))
                                                return <td>{obj[key].join(', ')}</td>
                                            return <td>{obj[key]}</td>;
                                        })
                                    }
                                </tr>
                            })
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

render(
    <App />, document.getElementById('root')
);